#!/bin/sh
rm -f app.jar
cd ..
./gradlew clean build -x check
cp build/libs/abdc*.jar docker/app.jar
cd docker
echo pulling images..
docker-compose pull
echo running
docker-compose up --build