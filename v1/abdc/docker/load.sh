#!/bin/sh

curl -X POST \
  http://localhost:8080/api/products \
  -H 'Content-Type: application/json' \
  -d '{
  "id": "CG7088",
  "name": "Nite Jogger Shoes",
  "model_number": "BTO93",
  "product_type": "inline",
  "meta_data": {
    "page_title": "adidas Nite Jogger Shoes - Black | adidas UK",
    "site_name": "adidas United Kingdom",
    "description": "Shop for Nite Jogger Shoes - Black at adidas.co.uk! See all the styles and colours of Nite Jogger Shoes - Black at the official adidas UK online store.",
    "keywords": "Nite Jogger Shoes",
    "canonical": "//www.adidas.co.uk/nite-jogger-shoes/CG7088.html"
  },
  "pricing_information": {
    "standard_price": 119.95,
    "standard_price_no_vat": 99.96,
    "currentPrice": 119.95
  },
  "product_description": {
    "title": "Nite Jogger Shoes",
    "subtitle": "Modern cushioning updates this flashy '\''80s standout.",
    "text": "Inspired by the 1980 Nite Jogger, these shoes shine bright with retro style and reflective details. The mesh and nylon ripstop upper is detailed with suede overlays. An updated Boost midsole adds responsive cushioning for all-day comfort."
  }
}'

curl -X POST \
  http://localhost:8080/api/products \
  -H 'Content-Type: application/json' \
  -d '{
  "id": "AG7022",
  "name": "Nite Jogger Shoes2",
  "model_number": "ATO22",
  "product_type": "inline",
  "meta_data": {
    "page_title": "adidas Nite Jogger Shoes - Black | adidas UK",
    "site_name": "adidas United Kingdom",
    "description": "Shop for Nite Jogger Shoes - Black at adidas.co.uk! See all the styles and colours of Nite Jogger Shoes - Black at the official adidas UK online store.",
    "keywords": "Nite Jogger Shoes",
    "canonical": "//www.adidas.co.uk/nite-jogger-shoes/AG7022.html"
  },
  "pricing_information": {
    "standard_price": 119.95,
    "standard_price_no_vat": 99.96,
    "currentPrice": 119.95
  },
  "product_description": {
    "title": "Nite Jogger Shoes",
    "subtitle": "Modern cushioning updates this flashy '\''80s standout.",
    "text": "Inspired by the 1980 Nite Jogger, these shoes shine bright with retro style and reflective details. The mesh and nylon ripstop upper is detailed with suede overlays. An updated Boost midsole adds responsive cushioning for all-day comfort."
  }
}'


curl -X POST \
  http://localhost:8080/api/products \
  -H 'Content-Type: application/json' \
  -d '{
  "id": "CG7033",
  "name": "Nite Jogger Shoes3",
  "model_number": "ATO33",
  "product_type": "inline",
  "meta_data": {
    "page_title": "adidas Nite Jogger Shoes - Black | adidas UK",
    "site_name": "adidas United Kingdom",
    "description": "Shop for Nite Jogger Shoes - Black at adidas.co.uk! See all the styles and colours of Nite Jogger Shoes - Black at the official adidas UK online store.",
    "keywords": "Nite Jogger Shoes",
    "canonical": "//www.adidas.co.uk/nite-jogger-shoes/CG7033.html"
  },
  "pricing_information": {
    "standard_price": 119.95,
    "standard_price_no_vat": 99.96,
    "currentPrice": 119.95
  },
  "product_description": {
    "title": "Nite Jogger Shoes",
    "subtitle": "Modern cushioning updates this flashy '\''80s standout.",
    "text": "Inspired by the 1980 Nite Jogger, these shoes shine bright with retro style and reflective details. The mesh and nylon ripstop upper is detailed with suede overlays. An updated Boost midsole adds responsive cushioning for all-day comfort."
  }
}'

curl -X POST \
  http://localhost:8080/api/products \
  -H 'Content-Type: application/json' \
  -d '{
  "id": "DG7044",
  "name": "Nite Jogger Shoes4",
  "model_number": "ATO44",
  "product_type": "inline",
  "meta_data": {
    "page_title": "adidas Nite Jogger Shoes - Black | adidas UK",
    "site_name": "adidas United Kingdom",
    "description": "Shop for Nite Jogger Shoes - Black at adidas.co.uk! See all the styles and colours of Nite Jogger Shoes - Black at the official adidas UK online store.",
    "keywords": "Nite Jogger Shoes",
    "canonical": "//www.adidas.co.uk/nite-jogger-shoes/DG7044.html"
  },
  "pricing_information": {
    "standard_price": 119.95,
    "standard_price_no_vat": 99.96,
    "currentPrice": 119.95
  },
  "product_description": {
    "title": "Nite Jogger Shoes",
    "subtitle": "Modern cushioning updates this flashy '\''80s standout.",
    "text": "Inspired by the 1980 Nite Jogger, these shoes shine bright with retro style and reflective details. The mesh and nylon ripstop upper is detailed with suede overlays. An updated Boost midsole adds responsive cushioning for all-day comfort."
  }
}'

curl -X POST \
  http://localhost:8080/api/products \
  -H 'Content-Type: application/json' \
  -d '{
  "id": "EG7055",
  "name": "Nite Jogger Shoes5",
  "model_number": "ATO55",
  "product_type": "inline",
  "meta_data": {
    "page_title": "adidas Nite Jogger Shoes - Black | adidas UK",
    "site_name": "adidas United Kingdom",
    "description": "Shop for Nite Jogger Shoes - Black at adidas.co.uk! See all the styles and colours of Nite Jogger Shoes - Black at the official adidas UK online store.",
    "keywords": "Nite Jogger Shoes",
    "canonical": "//www.adidas.co.uk/nite-jogger-shoes/EG7055.html"
  },
  "pricing_information": {
    "standard_price": 119.95,
    "standard_price_no_vat": 99.96,
    "currentPrice": 119.95
  },
  "product_description": {
    "title": "Nite Jogger Shoes",
    "subtitle": "Modern cushioning updates this flashy '\''80s standout.",
    "text": "Inspired by the 1980 Nite Jogger, these shoes shine bright with retro style and reflective details. The mesh and nylon ripstop upper is detailed with suede overlays. An updated Boost midsole adds responsive cushioning for all-day comfort."
  }
}'

