package com.adidas.abdc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class TestUtils {

    private static ObjectMapper MAPPER = new ObjectMapper();
    static {
        MAPPER.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
    }

    public static <T> T readFileObject(String fileName, Class<T> clazz) {
        try {
            return MAPPER.readValue(readFile(fileName), clazz);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String readFile(String fileName) {
        InputStream is = TestUtils.class.getClassLoader().getResourceAsStream(fileName);
        if (is != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            return reader.lines().collect(Collectors.joining(System.lineSeparator()));
        }
        throw new RuntimeException("no file " + fileName);
    }
}
