package com.adidas.abdc.e2e;

import com.adidas.abdc.TestUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = "spring.profiles.active=test", webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class End2EndIT {

    @ClassRule
    public static KafkaContainer kafka = new KafkaContainer().withEmbeddedZookeeper();

    @ClassRule
    public static PostgreSQLContainer pg = new PostgreSQLContainer().withUsername("adidas").withPassword("a4i4a5").withDatabaseName("adidas");

    @ClassRule
    public static ElasticsearchContainer es = new ElasticsearchContainer("docker.elastic.co/elasticsearch/elasticsearch-oss:6.4.1");

    @BeforeClass
    public static void before() {
        System.setProperty("PG_URL", pg.getJdbcUrl());
        System.setProperty("KFK_SERVERS", kafka.getBootstrapServers());
        System.setProperty("ES_SERVERS", es.getHttpHostAddress());
    }

    private final String productJson = TestUtils.readFile("samples/product.json");
    private final String productJson2 = TestUtils.readFile("samples/product2.json");

    @Test
    public void end2endTest_starting_with_empty_db_post_twice_and_check_searching() throws Exception {
        CloseableHttpClient client = HttpClients.createDefault();
        {
            HttpPost httpPost = new HttpPost("http://localhost:8080/api/products");
            httpPost.setEntity(new StringEntity(productJson, ContentType.APPLICATION_JSON));
            CloseableHttpResponse response = client.execute(httpPost);
            assertThat(response.getStatusLine().getStatusCode()).isEqualTo(202);
        }
        Thread.sleep(5000);
        {
            HttpGet httpGet = new HttpGet("http://localhost:8080/api/products");
            final CloseableHttpResponse response = client.execute(httpGet);
            String bodyAsString = EntityUtils.toString(response.getEntity());
            assertThat(response.getStatusLine().getStatusCode()).isEqualTo(200);
            assertThat(bodyAsString).contains("\"total_elements\":1,");
        }
        {
            HttpPost httpPost = new HttpPost("http://localhost:8080/api/products");
            httpPost.setEntity(new StringEntity(productJson2, ContentType.APPLICATION_JSON));
            CloseableHttpResponse response = client.execute(httpPost);
            assertThat(response.getStatusLine().getStatusCode()).isEqualTo(202);
        }
        Thread.sleep(2000);
        {
            HttpGet httpGet = new HttpGet("http://localhost:8080/api/products");
            final CloseableHttpResponse response = client.execute(httpGet);
            String bodyAsString = EntityUtils.toString(response.getEntity());
            assertThat(response.getStatusLine().getStatusCode()).isEqualTo(200);
            assertThat(bodyAsString).contains("\"total_elements\":2,");
        }
        {
            HttpGet httpGet = new HttpGet("http://localhost:8080/api/products?model_number=BTO93");
            final CloseableHttpResponse response = client.execute(httpGet);
            String bodyAsString = EntityUtils.toString(response.getEntity());
            assertThat(response.getStatusLine().getStatusCode()).isEqualTo(200);
            assertThat(bodyAsString).contains("\"total_elements\":1,");
        }
    }


}
