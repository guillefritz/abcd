package com.adidas.abdc.controller;

import com.adidas.abdc.TestUtils;
import com.adidas.abdc.listener.ProductEvents;
import com.adidas.abdc.model.product.Product;
import com.adidas.abdc.repository.ProductRepository;
import com.adidas.abdc.usecase.ProductCommands;
import com.adidas.abdc.usecase.ProductReads;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.hamcrest.core.StringContains;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductHttpApi.class)
public class ApiTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private ProductRepository productRepository;
    @MockBean
    private ProductEvents productEvents;
    @MockBean
    private ProductReads productReads;
    @MockBean
    private ProductCommands productCommands;

    private final Product product = TestUtils.readFileObject("samples/product.json", Product.class);

    @Test
    public void should_execute_search_filtering_by_product_type_and_paging() throws Exception {

        final Map<String,String> p = new HashMap<>();
        p.put("product_type", "inline");

        given(productReads.search(eq(p), eq(0), eq(20))).willReturn(Try.of(()-> new PageImpl<>(Collections.singletonList(product), PageRequest.of(1, 1), 1)));

        mockMvc.perform(get("/api/products?page=0&size=20&product_type=inline"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(StringContains.containsString(product.getId())));
    }

    @Test
    public void should_execute_search_by_id() throws Exception {

        given(productReads.findByID("CG7088")).willReturn(Option.of(product));

        mockMvc.perform(get("/api/products/CG7088"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(StringContains.containsString(product.getId())));
    }
}
