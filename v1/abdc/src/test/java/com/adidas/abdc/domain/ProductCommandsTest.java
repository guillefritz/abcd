package com.adidas.abdc.domain;

import com.adidas.abdc.TestUtils;
import com.adidas.abdc.listener.ProductEvents;
import com.adidas.abdc.repository.ProductRepository;
import com.adidas.abdc.model.product.Product;
import com.adidas.abdc.usecase.ProductCommands;
import io.vavr.control.Option;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class ProductCommandsTest {

    private final Product product = TestUtils.readFileObject("samples/product.json", Product.class);

    @Test
    public void given_a_create_command_should_insert_and_send_event() {

        //given
        final ProductEvents productEvents = mock(ProductEvents.class);
        final ProductRepository productRepository = mock(ProductRepository.class);
        final ArgumentCaptor<Product> argument = ArgumentCaptor.forClass(Product.class);
        when(productRepository.create(argument.capture())).thenReturn(product);

        final ProductCommands productCommands = new ProductCommands(productRepository, productEvents);

        //when
        final Product productCreated = productCommands.createProduct(product);

        //then
        assertThat(productCreated).isEqualTo(product);
        verify(productRepository).create(argument.capture());
        verify(productRepository, never()).findByID(any());
        verify(productRepository, never()).update(product);
    }

    @Test
    public void given_a_update_command_should_update_and_send_event() {

        //given
        final Product product = TestUtils.readFileObject("samples/product.json", Product.class);

        final ProductEvents productEvents = mock(ProductEvents.class);
        final ProductRepository productRepository = mock(ProductRepository.class);
        final ArgumentCaptor<Product> argument = ArgumentCaptor.forClass(Product.class);
        when(productRepository.findByID(product.getId())).thenReturn(Option.of(product));
        when(productRepository.update(argument.capture())).thenReturn(product);

        final ProductCommands productCommands = new ProductCommands(productRepository, productEvents);

        //when
        final Option<Product> productCreated = productCommands.updateProduct(product);

        //then
        assertThat(productCreated.isDefined()).isTrue();
        assertThat(productCreated.get()).isEqualTo(product);
        verify(productRepository).findByID(any());
        verify(productRepository).update(argument.capture());
        verify(productRepository, never()).create(product);
    }

}
