package com.adidas.abdc.listener;

import com.adidas.abdc.model.events.ProductEvent;
import org.springframework.kafka.support.SendResult;

import java.util.concurrent.CompletableFuture;

public interface ProductEvents {

    CompletableFuture<SendResult<String, Object>> sendEvent(ProductEvent productEvent);

}
