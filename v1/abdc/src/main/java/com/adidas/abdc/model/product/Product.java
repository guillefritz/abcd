package com.adidas.abdc.model.product;

//import static com.fasterxml.jackson.databind.PropertyNamingStrategy.SNAKE_CASE;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
//@Document(indexName = "products")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Product {

    @Id
    @Column(updatable = false, nullable = false, unique = true)
    private String id;
    @Column(nullable = false, unique = true)
    private String name;
    @Column(nullable = false, unique = true)
    private String modelNumber;
    @Column(nullable = false)
    private String productType;
    private MetaData metaData;
    private PricingInformation pricingInformation;
    private ProductDescription productDescription;

    public Product() {
    }

    public Product(String id, String name, String modelNumber, String productType, MetaData metaData, PricingInformation pricingInformation, ProductDescription productDescription) {
        this.id = id;
        this.name = name;
        this.modelNumber = modelNumber;
        this.productType = productType;
        this.metaData = metaData;
        this.pricingInformation = pricingInformation;
        this.productDescription = productDescription;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public PricingInformation getPricingInformation() {
        return pricingInformation;
    }

    public void setPricingInformation(PricingInformation pricingInformation) {
        this.pricingInformation = pricingInformation;
    }

    public ProductDescription getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(ProductDescription productDescription) {
        this.productDescription = productDescription;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", modelNumber='" + modelNumber + '\'' +
                ", productType='" + productType + '\'' +
                ", metaData=" + metaData +
                '}';
    }
}