package com.adidas.abdc.listener;

import com.adidas.abdc.model.events.ProductEvent;
import com.adidas.abdc.usecase.ProductCommands;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaProductEventListener  {

    private final ProductCommands productCommands;

    public KafkaProductEventListener(ProductCommands productCommands) {
        this.productCommands = productCommands;
    }

    @KafkaListener(topics = "${app.topics.productEvent}", groupId = "productEventDB")
    public void listenProductEventDBPersist(ProductEvent productEvent) {
        productCommands.handleProductEvent(productEvent);
    }
}
