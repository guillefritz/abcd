package com.adidas.abdc.controller;

import com.adidas.abdc.model.product.Product;
import com.adidas.abdc.usecase.ProductCommands;
import com.adidas.abdc.usecase.ProductReads;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Map;
import java.util.Optional;

@Api(value = "Products API")
@RestController
@RequestMapping("/api/products")
public class ProductHttpApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final ProductReads productReads;
    private final ProductCommands productCommands;

    public ProductHttpApi(ProductReads productReads, ProductCommands productCommands) {
        this.productReads = productReads;
        this.productCommands = productCommands;
    }


    /*
    Commands
    */
    @ApiResponse(code = 202, message = "Product create accepted and queued for creation")
    @PostMapping
    public ResponseEntity<Void> queueCreateProduct(@Valid @RequestBody Product product) {
        productCommands.queueCreateProduct(product);
        return ResponseEntity.accepted().build();
    }

    @ApiResponse(code = 202, message = "Product update accepted and queued for modification")
    @PutMapping("/{id}")
    public ResponseEntity<Void> queueUpdateProduct(@PathVariable String id, @Valid @RequestBody Product product) {
        productCommands.queueUpdateProduct(product);
        return ResponseEntity.accepted().build();
    }

    /*
        Reads
     */
    @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    @GetMapping("/{id}")
    public Optional<Product> findByID(@PathVariable String id) {
        return productReads.findByID(id).toJavaOptional();
    }

    @ApiResponse(code = 200, message = "Successfully retrieved list")
    @GetMapping(produces = "application/json")
    public ResponseEntity<Page<Product>> search(@RequestParam(required = false) Map<String,String> parameters,
                                                @RequestParam(required = false, name = "page", defaultValue = "0") Integer page,
                                                @RequestParam(required = false, name = "size", defaultValue = "10") Integer size) throws IOException {
        parameters.remove("page");
        parameters.remove("size");

        return productReads.search(parameters, page, size).fold(throwable -> {
            LOGGER.error("error", throwable);
            return ResponseEntity.status(500).build();
        }, ResponseEntity::ok);
    }
}
