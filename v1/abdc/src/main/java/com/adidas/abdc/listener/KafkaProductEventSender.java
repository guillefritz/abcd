package com.adidas.abdc.listener;

import com.adidas.abdc.model.events.ProductEvent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class KafkaProductEventSender implements ProductEvents {

    private final String productEventTopic;
    private final KafkaTemplate<String, Object> kafkaTemplate;

    public KafkaProductEventSender(@Value("${app.topics.productEvent}") String productEventTopic,
                                   KafkaTemplate<String, Object> kafkaTemplate) {
        this.productEventTopic = productEventTopic;
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public CompletableFuture<SendResult<String, Object>> sendEvent(ProductEvent productEvent) {
        return kafkaTemplate.send(productEventTopic, productEvent.getProduct().getId(), (productEvent)).completable();
    }
}
