package com.adidas.abdc.repository;

import com.adidas.abdc.model.product.Product;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface ProductRepository {

    Product create(Product product);

    Product update(Product product);

    void index(Product product);

    Option<Product> findByID(String id);

    Try<Page<Product>> search(Map<String, String> parameters, Integer page, Integer size);
}
