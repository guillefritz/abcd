package com.adidas.abdc.model.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Embeddable
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PricingInformation {

    private BigDecimal standardPrice;
    private BigDecimal standardPriceNoVat;
    @JsonProperty("currentPrice")
    private BigDecimal currentPrice;

    public PricingInformation() {
    }

    public PricingInformation(BigDecimal standardPrice, BigDecimal standardPriceNoVat, BigDecimal currentPrice) {
        this.standardPrice = standardPrice;
        this.standardPriceNoVat = standardPriceNoVat;
        this.currentPrice = currentPrice;
    }

    public BigDecimal getStandardPrice() {
        return standardPrice;
    }

    public void setStandardPrice(BigDecimal standardPrice) {
        this.standardPrice = standardPrice;
    }

    public BigDecimal getStandardPriceNoVat() {
        return standardPriceNoVat;
    }

    public void setStandardPriceNoVat(BigDecimal standardPriceNoVat) {
        this.standardPriceNoVat = standardPriceNoVat;
    }

    public BigDecimal getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(BigDecimal currentPrice) {
        this.currentPrice = currentPrice;
    }
}