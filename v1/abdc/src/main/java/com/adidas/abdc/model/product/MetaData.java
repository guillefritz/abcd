package com.adidas.abdc.model.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.persistence.Embeddable;

@Embeddable
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MetaData {

    private String pageTitle;
    private String siteName;
    private String description;
    private String keywords;
    private String canonical;

    public MetaData() {
    }

    public MetaData(String pageTitle, String siteName, String description, String keywords, String canonical) {
        this.pageTitle = pageTitle;
        this.siteName = siteName;
        this.description = description;
        this.keywords = keywords;
        this.canonical = canonical;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getCanonical() {
        return canonical;
    }

    public void setCanonical(String canonical) {
        this.canonical = canonical;
    }
}