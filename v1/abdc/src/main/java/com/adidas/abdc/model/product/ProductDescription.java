package com.adidas.abdc.model.product;

import javax.persistence.Embeddable;

@Embeddable
public class ProductDescription {

    private String title;
    private String subtitle;
    private String text;

    public ProductDescription() {
    }

    public ProductDescription(String title, String subtitle, String text) {
        this.title = title;
        this.subtitle = subtitle;
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
