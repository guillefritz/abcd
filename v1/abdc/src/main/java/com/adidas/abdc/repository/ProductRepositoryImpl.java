package com.adidas.abdc.repository;

import com.adidas.abdc.model.product.Product;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.matchQuery;

@Component
public class ProductRepositoryImpl implements ProductRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final ProductWriteRepository productWriteRepository;
    private final RestHighLevelClient esClient;
    private final ObjectMapper objectMapper;

    @Autowired
    public ProductRepositoryImpl(ProductWriteRepository productWriteRepository, RestHighLevelClient esClient, ObjectMapper objectMapper) {
        this.productWriteRepository = productWriteRepository;
        this.esClient = esClient;
        this.objectMapper = objectMapper;
    }

    @Override
    public Product create(Product product) {
        return productWriteRepository.save(product);
    }

    @Override
    public Product update(Product product) {
        return productWriteRepository.save(product);
    }

    @Override
    public void index(Product product) {
        try {
            byte[] bytes = objectMapper.writeValueAsBytes(product);
            esClient.index(new IndexRequest("products","products").id(product.getId()).source(bytes, XContentType.JSON), RequestOptions.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public Option<Product> findByID(String id) {
        return Option.ofOptional(productWriteRepository.findById(id));
    }

    @Override
    public Try<Page<Product>> search(Map<String, String> parameters, Integer page, Integer size) {
        return Try.of(() -> {
            final QueryBuilder query;
            if (parameters.isEmpty()) {
                query = QueryBuilders.matchAllQuery();
            } else {
                final BoolQueryBuilder q = QueryBuilders.boolQuery();
                parameters.forEach((key, value) -> q.must(matchQuery(key, value)));
                query = q;
            }

            SearchResponse response = esClient.search(new SearchRequest("products")
                    .source(new SearchSourceBuilder()
                            .query(query)
                            .from(page*size)
                            .size(size)
                            .trackTotalHits(true)
                    ), RequestOptions.DEFAULT);

            final List<Product> products = Arrays.stream(response.getHits().getHits()).map(SearchHit::getSourceAsString).map(s -> {
                try {
                    return objectMapper.readValue(s, Product.class);
                } catch (IOException e) {
                    LOGGER.error("error parsing json: "+s, e);
                    return null;
                }
            }).filter(Objects::nonNull)
                    .collect(Collectors.toList());

            return new PageImpl<Product>(products, PageRequest.of(page, size), products.size());
        });
    }
}
