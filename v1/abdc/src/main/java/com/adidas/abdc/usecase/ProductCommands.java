package com.adidas.abdc.usecase;

import com.adidas.abdc.listener.ProductEvents;
import com.adidas.abdc.repository.ProductRepository;
import com.adidas.abdc.model.events.ProductEvent;
import com.adidas.abdc.model.events.ProductEventType;
import com.adidas.abdc.model.product.Product;
import io.vavr.control.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import java.lang.invoke.MethodHandles;
import java.util.concurrent.CompletableFuture;

@Service
public class ProductCommands {

    private static final Logger LOGGER = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private final ProductRepository productRepository;
    private final ProductEvents productEvents;

    public ProductCommands(ProductRepository productRepository, ProductEvents productEvents) {
        this.productRepository = productRepository;
        this.productEvents = productEvents;
    }

    public CompletableFuture<SendResult<String, Object>> queueCreateProduct(Product product) {
        //validate
        return productEvents.sendEvent(new ProductEvent(ProductEventType.TO_CREATE, product));
    }

    public CompletableFuture<SendResult<String, Object>> queueUpdateProduct(Product product) {
        //validate
        return productEvents.sendEvent(new ProductEvent(ProductEventType.TO_UPDATE, product));
    }

    public void handleProductEvent(ProductEvent productEvent) {
        LOGGER.debug("handleProductEvent {}", productEvent);
        switch (productEvent.getProductEventType()) {
            case TO_CREATE:
                createProduct(productEvent.getProduct());
                break;
            case TO_UPDATE:
                updateProduct(productEvent.getProduct());
                break;
            case CREATED:
            case UPDATED:
                productRepository.index(productEvent.getProduct());
                break;
        }
    }

    public Product createProduct(Product product) {
        Product productCreated = productRepository.create(product);
        productEvents.sendEvent(new ProductEvent(ProductEventType.CREATED, productCreated));
        return productCreated;
    }

    public Option<Product> updateProduct(Product product) {
        return productRepository.findByID(product.getId())
                .map(productFound -> {
                    final Product productUpdated = productRepository.update(product);
                    productEvents.sendEvent(new ProductEvent(ProductEventType.UPDATED, productUpdated));
                    return Option.of(productUpdated);
                }).getOrElse(Option::none);
    }
}
