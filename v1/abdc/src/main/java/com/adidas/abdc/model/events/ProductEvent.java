package com.adidas.abdc.model.events;

import com.adidas.abdc.model.product.Product;

import java.time.OffsetDateTime;

public class ProductEvent {

    private ProductEventType productEventType;
    private Product product;

    public ProductEvent() {}

    public ProductEvent(ProductEventType productEventType, Product product) {
        this.productEventType = productEventType;
        this.product = product;
    }

    public ProductEvent(ProductEventType productEventType, Product product, OffsetDateTime eventDateTime) {
        this.productEventType = productEventType;
        this.product = product;
    }

    public ProductEventType getProductEventType() {
        return productEventType;
    }

    public void setProductEventType(ProductEventType productEventType) {
        this.productEventType = productEventType;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "ProductEvent{" +
                "productEventType=" + productEventType +
                ", product=" + product +
                '}';
    }
}
