package com.adidas.abdc.usecase;

import com.adidas.abdc.repository.ProductRepository;
import com.adidas.abdc.model.product.Product;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ProductReads {

    private final ProductRepository productRepository;

    public ProductReads(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Option<Product> findByID(String id) {
        return productRepository.findByID(id);
    }

    public Try<Page<Product>> search(Map<String, String> parameters, Integer page, Integer size) {
        return productRepository.search(parameters, page, size);
    }
}
