package com.adidas.abdc.repository;

import com.adidas.abdc.model.product.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductWriteRepository extends JpaRepository<Product, String> {
}
