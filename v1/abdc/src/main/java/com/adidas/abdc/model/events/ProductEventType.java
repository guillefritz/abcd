package com.adidas.abdc.model.events;

public enum ProductEventType {

    TO_CREATE, TO_UPDATE,
    CREATED, UPDATED
}
