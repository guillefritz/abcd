# abdc (adidas backend development challenge)

## versions

### v1: spring boot

please check at v1/abdc, this version made with java+springboot has the following features:

- Postgres DB as RDBMS accesed by Spring Data JPA
- Elasticsearch for faster search API done by Elasticsearch's High Level Rest API
- Kafka as log messaging layer

#### Use cases:

1. write API: handle POST & PUT http requests, and emit an message (as a command) to a Kafka topic with the type TO_CREATE/TO_UPDATE
2. kafka listener: listen for events.
    * TO_CREATE/TO_UPDATE: do persistence into Postgres DB, also emits another event to the same topic with type CREATED/UPDATED
    * CREATED/UPDATED: take this kind of events and save the product content into Elasticsearch index.
3. read API: handle search GET requests and then do search using Elasticsearch high level rest api.


### other versions

I'd liked to solve using different architecture like:

v2: outbox pattern
- write API ms (using micronaut, kotlin to enqueue to Kafka event)
- Kafka listener to persist into Postgres products & message tables in the same tx
- job process to read messages tables, index to Elasticsearch & mark row as processed
- Read API ms (micronaut & elasticsearch access)

v3: using Debezium 
- Write API ms (using micronaut, kotlin to enqueue to Kafka event)
- Kafka listener to persist into MongoDB products collection 
- Use Debezium to read the MongoDB OPLOG (we also could use Postgres->WAL) and produces Kafka events as CDC (change data capture)
- Use Kafka sink connector to persist events into Elasticsearch index
- Read API ms (micronaut & elasticsearch access)



#### Build steps

##### Requisites
- docker & docker-compose
- linux

##### Run end to end Integration Test

    ./gradlew clean check
    
Most important End2EndIT it's an entire test 

##### Runing Stack

    cd v1/abdc/docker
    chmod +x *.sh
    ./run.sh
    
This sh file will:
 - build jar using gradle, 
 - start a docker-compose stack (v1/abdc/docker/docker-compose.yml) will take a while to pull imges!

##### Swagger

    http://localhost:8080/swagger-ui.html

 
##### Loading data

this script will load some products

    ./load.sh

##### searching API

base search

    curl -v http://localhost:8080/api/products
    
all terms can be used as filters

    curl -v http://localhost:8080/api/products?product_type=inline
    
**size** will set the page size (default 10)
**page** will set the page number (default 0) 

    curl -v http://localhost:8080/api/products?size=2&page=0
 
##### Cleaning
 
    ./clean.sh